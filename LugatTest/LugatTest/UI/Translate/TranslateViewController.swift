//
//  TranslateViewController.swift
//  LugatTest
//
//  Created by Alexandr Shevchenko on 21/09/2019.
//  Copyright © 2019 Alexandr Shevchenko. All rights reserved.
//
import UIKit
import Foundation

class TranslateViewController: UIViewController, UITextFieldDelegate {
    
    var presenter: TranslatePresenterDelegate!
    var tableDataSource: TableDataSourceDelegate!
    @IBOutlet weak var inputTextWord: UITextField!{
        didSet{
            self.inputTextWord.delegate = self
        }
    }
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            self.tableView.rowHeight = UITableView.automaticDimension
            self.tableView.estimatedRowHeight = 600
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableDataSource = TableDataSource()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
   
    @IBAction func edidChange(_ sender: Any) {
        presenter.editChancheWord(self.inputTextWord.text!)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.inputTextWord.resignFirstResponder()
        return true
    }
    
}

extension TranslateViewController: TranslateViewDelegate{
    func updateTable(_ array: [Word]) {
        self.tableDataSource.setWords(array)
        self.tableView.reloadData()
    }
}
extension TranslateViewController: UITableViewDelegate{
    
}

extension TranslateViewController: UITableViewDataSource{
    
//    func numberOfSections(in tableView: UITableView) -> Int {
//        presenter.getNumberOfWords()
//    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tableDataSource.getWordsCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WordCell") as! WordTableViewCell
        cell.setInfo(word: self.tableDataSource.getWordId(indexPath.row))
        return cell
    }
    
    
}
