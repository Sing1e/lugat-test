//
//  TranslateContract.swift
//  LugatTest
//
//  Created by Alexandr Shevchenko on 22/09/2019.
//  Copyright © 2019 Alexandr Shevchenko. All rights reserved.
//
import UIKit
import Foundation

protocol TranslateViewDelegate{
    func updateTable(_ array: [Word])
}
protocol TranslatePresenterDelegate{
    var view: TranslateViewDelegate! { get set }
    func editChancheWord(_ word: String)
}

protocol TranslateModelDelegate{
    func updateWords(_ word: String, response: @escaping ([Word]) -> Void)
}

protocol TableDataSourceDelegate{
    func getWordsCount() -> Int
    func getWordId(_ id: Int) -> Word
    func setWords(_ data: [Word])
}
