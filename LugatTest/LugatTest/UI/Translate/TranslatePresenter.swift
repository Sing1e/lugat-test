//
//  TranslatePresenter.swift
//  LugatTest
//
//  Created by Alexandr Shevchenko on 22/09/2019.
//  Copyright © 2019 Alexandr Shevchenko. All rights reserved.
//

import Foundation
class TranslatePresenter: TranslatePresenterDelegate{
    
    
    
    var view: TranslateViewDelegate!
    var model: TranslateModelDelegate!
    
    init(model: TranslateModelDelegate) {
        self.model = model
    }
    
    func editChancheWord(_ word: String){
        model.updateWords(word) { (array) in
            self.view.updateTable(array)
        }
        
    }
    
}
