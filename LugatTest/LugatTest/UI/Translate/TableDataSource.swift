//
//  TableDataSource.swift
//  LugatTest
//
//  Created by Alexandr Shevchenko on 23/09/2019.
//  Copyright © 2019 Alexandr Shevchenko. All rights reserved.
//

import Foundation

class TableDataSource: TableDataSourceDelegate{
    var data: [Word] = []
    
    func getWordsCount() -> Int {
        data.count
    }
    
    func getWordId(_ id: Int) -> Word {
        data[id]
    }
       
    func setWords(_ data: [Word]) {
        self.data = data
    }
    
    
}
