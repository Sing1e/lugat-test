//
//  TranslateModel.swift
//  LugatTest
//
//  Created by Alexandr Shevchenko on 22/09/2019.
//  Copyright © 2019 Alexandr Shevchenko. All rights reserved.
//

import Foundation

class TranslateModel: TranslateModelDelegate{
    var dataFetcher: DataFetcher!
    init() {
        self.dataFetcher = LocalDataFetcher()
    }
    func updateWords(_ word: String, response: @escaping ([Word]) -> Void) {
        getData { (data) in
            var words: [Word] = []
            for index in data!.words {
                if index.original.lowercased().hasPrefix(word.lowercased()) != false{
                    words.append(index)
                }
            }
            response(words)
        }
    }
    
    func getData(completion: @escaping (TranslateData?) -> Void) {
        let localUrlString = "ru_crt.json"
        dataFetcher.fetchGenericJSONData(urlString: localUrlString, response: completion)
    }
     
    
}
