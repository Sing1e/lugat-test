//
//  WordTableViewCell.swift
//  LugatTest
//
//  Created by Alexandr Shevchenko on 22/09/2019.
//  Copyright © 2019 Alexandr Shevchenko. All rights reserved.
//
import UIKit
import Foundation
class WordTableViewCell: UITableViewCell{
    
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var translateLabel: UILabel!
    
    var shapeLayer: CAShapeLayer!{
        didSet{
            self.shapeLayer.lineWidth = 2
            self.shapeLayer.lineCap = CAShapeLayerLineCap(rawValue: "round")
            self.shapeLayer.fillColor = nil
            self.shapeLayer.strokeEnd = 1
            let color = #colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1).cgColor
            self.shapeLayer.strokeColor = color
            
            
        }
    }
    
    var gradientLayer: CAGradientLayer!{
        didSet{
            self.gradientLayer.startPoint = CGPoint(x: 1, y: 0)
            self.gradientLayer.endPoint = CGPoint(x: 0, y: 1)
            let color1 = #colorLiteral(red: 0.2588235438, green: 0.7568627596, blue: 0.9686274529, alpha: 1).cgColor
            let color2 = #colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1).cgColor
            self.gradientLayer.colors = [color1,color2]
        }
    }
    
    override func layoutSubviews() {
        self.gradientLayer.frame = CGRect(x: 0, y: 0, width: contentView.frame.width, height: contentView.frame.height)
        
        self.shapeLayer.frame = contentView.bounds
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 20, y: 38))
        path.addLine(to: CGPoint(x: contentView.frame.maxX-20, y: 38))
        self.shapeLayer.path = path.cgPath
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.contentView.layer.cornerRadius = 10
        self.gradientLayer = CAGradientLayer()
        self.contentView.layer.insertSublayer(gradientLayer, at: 0)
        
        self.shapeLayer = CAShapeLayer()
        self.contentView.layer.addSublayer(shapeLayer)
        
        self.contentView.layer.borderWidth = 2
//        let color = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1).cgColor
        
        let color = UIColor(named: "borderColor")?.cgColor
        self.contentView.layer.borderColor = color
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)
    }
    
    func setInfo(word: Word){
        self.labelTitle.text = word.original
        self.translateLabel.text = word.translated
        
    }
    
    
}
