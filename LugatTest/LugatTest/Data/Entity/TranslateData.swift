//
//  TranslateData.swift
//  LugatTest
//
//  Created by Alexandr Shevchenko on 22/09/2019.
//  Copyright © 2019 Alexandr Shevchenko. All rights reserved.
//

import Foundation
struct TranslateData: Decodable{
    let words: [Word]
}
struct Word: Decodable{
    let original: String
    let translated: String
}
