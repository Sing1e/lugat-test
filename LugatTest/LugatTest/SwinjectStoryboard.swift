//
//  SwinjectStoryboard.swift
//  LugatTest
//
//  Created by Alexandr Shevchenko on 23/09/2019.
//  Copyright © 2019 Alexandr Shevchenko. All rights reserved.
//
import Swinject
import SwinjectStoryboard
import Foundation
extension SwinjectStoryboard{
    @objc class func setup() {
        Container.loggingFunction = nil
        
        defaultContainer.register(TranslateModel.self) { _ in TranslateModel()}
        
        defaultContainer.register(TranslatePresenterDelegate.self) { _ in TranslatePresenter(
            model: defaultContainer.resolve(TranslateModel.self)! )}
        
        defaultContainer.storyboardInitCompleted(TranslateViewController.self) {
            (resolver, controller) in
            var presenter = resolver.resolve(TranslatePresenterDelegate.self)
            presenter?.view = controller
            controller.presenter = presenter
            
        }
    }
}
